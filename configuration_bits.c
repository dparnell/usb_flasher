/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#ifdef __XC32
    #include <xc.h>          /* Defines special funciton registers, CP0 regs  */
#endif

#include <plib.h>            /* Include to use PIC32 peripheral libraries     */

/******************************************************************************/
/* Configuration Bits                                                         */
/*                                                                            */
/* Refer to 'C32 Configuration Settings' under the Help > Contents            */
/* > C32 Toolchain in MPLAB X IDE for available C32 PIC32 Configurations. For */
/* additional information about what the hardware configurations mean in      */
/* terms of device operation, refer to the device datasheet 'Special Features'*/
/* chapter. XC32 documentation is available under Help > Contents >           */
/* XC32 C Compiler.                                                           */
/*                                                                            */
/* A feature of MPLAB X is the 'Generate Source Code to Output' utility in    */
/* the Configuration Bits window.  Under Window > PIC Memory Views >          */
/* Configuration Bits, a user controllable configuration bits window is       */
/* available to Generate Configuration Bits source code which the user can    */
/* paste into this project.                                                   */
/******************************************************************************/

// Configuration Bit settings
// SYSCLK = 40 MHz (8MHz Crystal / FPLLIDIV * FPLLMUL / FPLLODIV)
// PBCLK = 40 MHz (SYSCLK / FPBDIV)
// Primary Osc w/PLL (XT+,HS+,EC+PLL)
#pragma config FPLLMUL = MUL_20, FPLLIDIV = DIV_2, FPLLODIV = DIV_1
#pragma config POSCMOD = HS, FNOSC = PRIPLL, FPBDIV = DIV_1
#pragma config ICESEL   = ICS_PGx3      // ICE/ICD Comm Channel Select
#pragma config FWDTEN = OFF				// WDT OFF
// USB
#pragma config UPLLEN   = ON        	// USB PLL Enabled
#pragma config UPLLIDIV = DIV_2         // USB PLL Input Divider (8MHz Crystal / 2 * 24 /2)
#pragma config FUSBIDIO = OFF			// USB USID - Controlled by the port pin
#pragma config FVBUSONIO = OFF			// USB VBUS ON - Controlled by the port pin

#pragma config FSOSCEN = OFF			// Secondary Oscillator Enab

#pragma config JTAGEN = OFF				// JTAG Disabled
#pragma config DEBUG = OFF