//************************************************************************
//	cdcacm.h

#ifndef __CDCACM_H__
#define __CDCACM_H__

#include "usb.h"

extern bool cdcacm_active;

typedef void (*cdcacm_reset_cbfn)(void);
void	cdcacm_print(const byte *line, int length);
void	cdcacm_command_ack(void);
void	cdcacm_register(cdcacm_reset_cbfn reset);  // XXX -- register receive upcall!

extern bool application_receive(const byte *buffer, int length);
#endif