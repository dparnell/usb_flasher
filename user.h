/******************************************************************************/
/* User Level #define Macros                                                  */
/******************************************************************************/

#define SAMPLE_RATE     5000        // 200us interval: Enough time for 1440 instructions at 72Mhz.
#define FPB                    40000000
#define MAX_PWM           FPB/SAMPLE_RATE // eg 7200 at 5000hz.

/******************************************************************************/
/* User Function Prototypes                                                    /
/******************************************************************************/

/* TODO User level functions prototypes (i.e. InitApp) go here */

void InitApp(void);         /* I/O and Peripheral Initialization */


