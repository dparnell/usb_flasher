/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#include <plib.h>            /* Include to use PIC32 peripheral libraries     */
#include <stdint.h>          /* For uint32_t definition                       */
#include <stdbool.h>         /* For true/false definition                     */
#include "user.h"            /* variables/params used by user.c               */

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

void InitApp(void)
{
    SYSTEMConfigPerformance(80000000L);  // This function sets the PB-Div to 1. Also optimises cache for 80Mhz etc..
    mOSCSetPBDIV(OSC_PB_DIV_2);           // Therefore, configure the PB bus to run at 1/2 CPU Frequency

    // enable writing to the pin mapping registers
    SYSKEY = 0xAA996655;            // Write Key1 to SYSKEY
    SYSKEY = 0x556699AA;            // Write Key2 to SYSKEY

    RTCCON = 0;  // make sure the RTCC is disabled
    
    // configure the port pins as needed
    RPB0R = 0;
    RPB1R = 0;
    RPB2R = 0;
    RPB3R = 0;
    RPB4R = 0;

    RPB7R = 0b0101; // OC1
    RPB8R = 0b0101; // OC2
    RPB9R = 0b0101; // 0C3

    SYSKEY = 0;

    // All port B pins as outputs and all LEDs off
    LATB  = 0xffff;
    TRISB = 0;
    
    // configure the OCx modules for PWM output
    OpenOC1(OC_ON | OC_TIMER2_SRC | OC_PWM_FAULT_PIN_DISABLE, 0, 0); // init OC1 module, T2 =source
    OpenOC2(OC_ON | OC_TIMER2_SRC | OC_PWM_FAULT_PIN_DISABLE, 0, 0); // init OC2 module, T2 =source
    OpenOC3(OC_ON | OC_TIMER2_SRC | OC_PWM_FAULT_PIN_DISABLE, 0, 0); // init OC3 module, T2 =source
    
    OpenTimer2(T2_ON | T2_PS_1_1 | T2_SOURCE_INT, FPB/SAMPLE_RATE);         // init Timer2 mode and period reg (PR2)
 }
