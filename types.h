/* 
 * File:   types.h
 * Author: daniel
 *
 * Created on February 13, 2013, 4:14 PM
 */

#ifndef TYPES_H
#define	TYPES_H

#ifdef	__cplusplus
extern "C" {
#endif


#define asm_halt()  asm("SDBBP");

#include <plib.h>
typedef char int8;
typedef unsigned char uint8;
typedef short int16;
typedef unsigned short uint16;
typedef int int32;
typedef unsigned int uint32;
typedef int intptr;
typedef unsigned int uintptr;

typedef unsigned char bool;
typedef unsigned char byte;
typedef unsigned int uint;

enum {
    false,
    true
};

#define IN
#define OUT
#define OPTIONAL
#define VARIABLE  1
#define MIN(a, b)  ((a) < (b) ? (a) : (b))
#define MAX(a, b)  ((a) > (b) ? (a) : (b))
#define ROUNDUP(n, s)  (((n)+(s)-1)&~((s)-1))  // N.B. s must be power of 2!
#define ROUNDDOWN(n, s)  ((n)&~((s)-1))  // N.B. s must be power of 2!
#define LENGTHOF(a)  (sizeof(a)/sizeof(a[0]))
#define OFFSETOF(t, f)  ((int)(intptr)(&((t *)0)->f))
#define IS_POWER_OF_2(x) ((((x)-1)&(x))==0)

#if SODEBUG
	#define assert(x)  do { if (! (x)) { asm_halt(); } } while (0)
#else
	#define assert(x)
#endif

#ifdef _DEBUG_VIA_SERIAL_
	#define ASSERT(x)  do { if (! (x)) { Serial_print("calling asm_halt"); asm_halt(); } } while (0)
#else
	#define ASSERT(x)  do { if (! (x)) { asm_halt(); } } while (0)
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* TYPES_H */

