/******************************************************************************/
/*  Files to Include                                                          */
/******************************************************************************/

#include <plib.h>           /* Include to use PIC32 peripheral libraries      */

#include "system.h"         /* System funct/params, like osc/periph config    */
#include "user.h"           /* User funct/params, such as InitApp             */
#include "usb.h"
#include "cdcacm.h"

/******************************************************************************/
/* Global Variable Declaration                                                */
/******************************************************************************/

/* i.e. uint32_t <variable_name>; */

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

#define LED_COUNT 5

int r[LED_COUNT];
int g[LED_COUNT];
int b[LED_COUNT];

// #define TEST_LEDS
#ifdef TEST_LEDS
int dr[LED_COUNT];
int dg[LED_COUNT];
int db[LED_COUNT];
#endif


void testPattern() {
    r[0] = 0xffff;
    g[0] = 0x0000;
    b[0] = 0x0000;

    r[1] = 0x0000;
    g[1] = 0xffff;
    b[1] = 0x0000;

    r[2] = 0x0000;
    g[2] = 0x0000;
    b[2] = 0xffff;

    r[3] = 0xffff;
    g[3] = 0xffff;
    b[3] = 0x0000;

    r[4] = 0xffff;
    g[4] = 0x0000;
    b[4] = 0xffff;

}

void clearLEDs() {
    int i;

    for(i=0; i<LED_COUNT; i++) {
        r[i] = 0;
        g[i] = 0;
        b[i] = 0;
    }
}

int state = 0;
int sub_state = 0;
int x, y;
short red, green, blue;

enum {
    STATE_GROUND = 0,
    STATE_SET
};

const char info_message[] = "1x5 RGB Led Array v0.1 by Daniel Parnell <me@danielparnell.com>\r\n";
const char ok_message[] = "OK\r\n";
const char error_message[] = "ERROR\r\n";
const char help_message[] = "\r\nCommands:\r\n\
\tI - Information\r\n\
\tC - Clear\r\n\
\tT - Test pattern\r\n\
\tSx,yRRRR,GGGG,BBBB - Set LED\r\n\
\t? - This help\r\n\r\n";

void handle_zero_state(byte ch) {
    switch(ch) {
        case '?':
            cdcacm_print(info_message, sizeof(info_message));
            cdcacm_print(help_message, sizeof(help_message));
            return;
        case 'I':
            cdcacm_print(info_message, sizeof(info_message));
            return;
        case 'C':
            clearLEDs();
            break;
        case 'T':
            testPattern();
            break;

        case 'S':
            state = STATE_SET;
            sub_state = 0;
            x = 0;
            y = 0;
            return;
        default:
            cdcacm_print(error_message, sizeof(error_message));
            return;
    }

    cdcacm_print(ok_message, sizeof(ok_message));
}

enum {
    SET_X = 0,
    SET_Y,
    SET_RED,
    SET_GREEN,
    SET_BLUE,
    SET_DONE
};

void handle_hex(short* value, byte ch) {
    if((ch>='0' && ch<='9') || (ch>='A' && ch<='F')) {
        short val = *value << 4;

        ch = ch - '0';
        if(ch > 9) {
            ch = ch - 8;
        }
        *value = val | ch;
    } else {
        if(ch == ' ' || ch == ',' || ch =='\r' || ch == '\n') {
            sub_state++;

            if(sub_state == SET_DONE) {
                r[y] = red;
                g[y] = green;
                b[y] = blue;

                state = STATE_GROUND;
                cdcacm_print(ok_message, sizeof(ok_message));
            }
        } else {
            state = STATE_GROUND;
            cdcacm_print(error_message, sizeof(error_message));
        }
    }
}

void handle_set(byte ch) {
    switch(sub_state) {
        case SET_X:
            if(ch=='0') {
                x = ch - '0';
                sub_state = SET_Y;
            } else {
                state = STATE_GROUND;
                cdcacm_print(error_message, sizeof(error_message));
                return;
            }
            break;

        case SET_Y:
            if(ch==',' || ch==' ') {
                return;
            }
            if(ch>='0' && ch<='4') {
                y = ch - '0';
                sub_state = SET_RED;
                red = 0;
                green = 0;
                blue = 0;
            } else {
                state = STATE_GROUND;
                cdcacm_print(error_message, sizeof(error_message));
                return;
            }
            break;

        case SET_RED:
            handle_hex(&red, ch);
            break;
        case SET_GREEN:
            handle_hex(&green, ch);
            break;
        case SET_BLUE:
            handle_hex(&blue, ch);
            break;
    }
}

bool application_receive(const byte *buffer, int length)
{
	int i;

	for (i	= 0; i < length; i++)
	{
            switch(state) {
                case STATE_GROUND:
                    handle_zero_state(buffer[i]);
                    break;

                case STATE_SET:
                    handle_set(buffer[i]);
                    break;
            }
	}

	return true;
}


int led = 0;

static void app_reset_cbfn(void)
{
    // DO NOTHING
}

int32 main(void)
{
#ifdef TEST_LEDS
    int i;

    for(i=0; i<LED_COUNT; i++) {
        r[i] = rand() % SAMPLE_RATE;
        g[i] = rand() % SAMPLE_RATE;
        b[i] = rand() % SAMPLE_RATE;

        dr[i] = (rand() & 1 ? -1 : 1) * ((rand() % 4)+1);
        dg[i] = (rand() & 1 ? -1 : 1) * ((rand() % 4)+1);
        db[i] = (rand() & 1 ? -1 : 1) * ((rand() % 4)+1);
    }

    r[2] = 0;
    dr[2] = 0;
    g[2] = 0;
    dg[2] = 0;
    b[2] = MAX_PWM;
    db[2] = 100;

    r[3] = 0;
    dr[3] = 0;
    g[3] = MAX_PWM/2;
    dg[3] = 100;
    b[3] = 0;
    db[3] = 0;

    r[4] = 0;
    dr[4] = 100;
    g[4] = 0;
    dg[4] = 0;
    b[4] = 0;
    db[4] = 0;
#else
    testPattern();
#endif

#ifndef PIC32_STARTER_KIT
    /*The JTAG is on by default on POR.  A PIC32 Starter Kit uses the JTAG, but
    for other debug tool use, like ICD 3 and Real ICE, the JTAG should be off
    to free up the JTAG I/O */
    DDPCONbits.JTAGEN = 0;
#endif

    /*Refer to the C32 peripheral library compiled help file for more
    information on the SYTEMConfig function.
    
    This function sets the PB divider, the Flash Wait States, and the DRM
    /wait states to the optimum value.  It also enables the cacheability for
    the K0 segment.  It could has side effects of possibly alter the pre-fetch
    buffer and cache.  It sets the RAM wait states to 0.  Other than
    the SYS_FREQ, this takes these parameters.  The top 3 may be '|'ed
    together:
    
    SYS_CFG_WAIT_STATES (configures flash wait states from system clock)
    SYS_CFG_PB_BUS (configures the PB bus from the system clock)
    SYS_CFG_PCACHE (configures the pCache if used)
    SYS_CFG_ALL (configures the flash wait states, PB bus, and pCache)*/

    /* TODO Add user clock/system configuration code if appropriate.  */
    SYSTEMConfig(SYS_FREQ, SYS_CFG_ALL); 

    /* Initialize I/O and Peripherals for application */
    InitApp();

    /*Configure Multivector Interrupt Mode.  Using Single Vector Mode
    is expensive from a timing perspective, so most applications
    should probably not use a Single Vector Mode*/
    INTConfigureSystem(INT_SYSTEM_CONFIG_MULT_VECTOR);
    INTEnableSystemMultiVectoredInt();

    mT2SetIntPriority(7); 	// set Timer2 Interrupt Priority
    mT2ClearIntFlag(); 		// clear interrupt flag
    mT2IntEnable(1);		// enable timer2 interrupts

    // initialize usb
    usb_initialize();
    cdcacm_register(app_reset_cbfn);

    while(1)
    {
#if ! INTERRUPT
        // we poll the USB periodically
        mT2IntEnable(0);
	usb_isr();
        mT2IntEnable(1);
#endif
    }
}

int counter = 1;

void __ISR( _TIMER_2_VECTOR, IPL7AUTO) T2Interrupt( void) {
    if(counter-- <= 0) {
        counter = 10;

        led++;
        if(led >= LED_COUNT) {
            led = 0;
        }

#ifdef TEST_LEDS
        r[led] += dr[led];
        g[led] += dg[led];
        b[led] += db[led];

        if(r[led]<0) {
            r[led] = 0;
            dr[led] = -dr[led];
        }
        if(r[led]>MAX_PWM) {
            r[led] = MAX_PWM;
            dr[led] = -dr[led];
        }
        if(g[led]<0) {
            g[led] = 0;
            dg[led] = -dg[led];
        }
        if(g[led]>MAX_PWM) {
            g[led] = MAX_PWM;
            dg[led] = -dg[led];
        }
        if(b[led]<0) {
            b[led] = 0;
            db[led] = -db[led];
        }
        if(b[led]>MAX_PWM) {
            b[led] = MAX_PWM;
            db[led] = -db[led];
        }
#endif

        // Set the PWM outputs
        SetDCOC1PWM(r[led]);
        SetDCOC2PWM(g[led]);
        SetDCOC3PWM(b[led]);

        // The new PWM values will not take effect until the next time the timer interrupt happens, so we will change the enabled LEDs then

    } else {
        // change the enabled LEDs
        LATB = 0x1f ^ (1 << led);        
    }

    mT2ClearIntFlag();
}